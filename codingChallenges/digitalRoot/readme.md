# [🏆🏆CHALLENGE🏆🏆] [Digital Root](https://www.sololearn.com/Discuss/814764/challenge-digital-root)
### What is digital root?
The digital root is a value obtained by an iterative process of summing digits, on each iteration using the result from the previous iteration to compute a digit sum. The process continues until a single-digit number is reached. The number should be positive.
Eg. 1234 -> 1 + 2 + 3 + 4 = 10 -> 1 + 0 = (1)

Write a code to find the digital root of a number.
