# [M💙 Challenge] [LED Clock Brightness](https://www.sololearn.com/Discuss/824362/m-challenge-cotd-led-clock-brightness) 🔆🔆🔆
#### Monday Blue 💙 Challenge Series #7
Let's work against the clock this week! This time we will need to calculate the brightness of the LED clock based on the time display in 24-hour format. A conventional LED clock use 7 segments to display each character and therefore the brightness will range from 1 to 7.
#### 💼 TASK
Write a program to determine the time (hh:mm:ss) on any day in which the LED clock have the lowest and highest brightness respectively. The LED clock display the time in following format.
#### { Hour } : { Minute } : { Second }
Here's the brightness of each digit represented in LED:
* 1⃣  🔆 2
* 2⃣  🔆 5
* 3⃣  🔆 5
* 4⃣  🔆 4
* 5⃣  🔆 5
* 6⃣  🔆 6
* 7⃣  🔆 3
* 8⃣  🔆 7
* 9⃣  🔆 6
* 0⃣  🔆 6
#### 🔧 EXAMPLE
* 00 : 00 : 00  🔆 36
* 11 : 53 : 22 🔆 24
* 22 : 25 : 00 🔆 32
#### ▶ OUTPUT
* Brightness { brightness } (lowest) @ { hh:mm:ss }
* Brightness { brightness } (highest) @ { hh:mm:ss }
#### ✔ HINT
This challenge requires no input and determine your encoding and problem solving skill. You may utilize the built-in DateTime library from the language of your choice but it's not necessary. 😉
#### ❤ BONUS
Creative and clear-cut approach
