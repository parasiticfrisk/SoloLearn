#!/usr/bin/env python3
# August Frisk | (c) 2017
# LED Clock Brightness Challenge

led={'1':2,'2':5,'3':5,'4':4,'5':5, '6':6,'7':3,'8':7,'9':6,'0':6}
def time(strng):
    val=0
    for i in strng:
       if i!=':':
          val+=led[i]
    return val
max,min=0,100      
for h in range(24): #hours
    for m in range(60): #minutes
        for s in range(60): #seconds
            dsply=str(h).zfill(2)+':'+str(m).zfill(2)+':'+str(s).zfill(2)
            clck=time(dsply)
            if clck>max:
               max=clck
               maxT=dsply
            elif clck<min:
               min=clck
               minT=dsply
print('Maximum brightness level',max,', at',maxT,'\nMinimum brightness level',min,', at',minT)
