#!/usr/bin/env python3
# August Frisk | (c) 2017
# Remove Vowels Challege

[print(x, end='') for x in input() if not x in "aeiouAEIOU"]
