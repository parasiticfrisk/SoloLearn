#!/usr/bin/env python3
# Scrabble Challenge
# August Frisk | (c) 2018

# dictionary to define letter's points value
points={'a':1,'e':1,'i':1,'o':1,'u':1,'l':1,'n':1,'r':1,'s':1,'t':1,'d':2,'g':2,'b':3,'c':3,'m':3,'p':3,'f':4,'h':4,'v':4,'w':4,'y':4,'k':5,'j':8,'x':8,'q':10,'z':10}

# function to calculate score
def getScore(word):
    score=0
    for letter in list(word.lower()):
        if letter in points:
            score+=points[letter]
    return score
    
# split input words to get letter value
words=input().split(' ')
bestWord=('', 0)

for word in words:
    score=getScore(word)
    if score > bestWord[1]:
        bestWord=(word, score)
        
print('The best word is "'+bestWord[0]+'" with a score of: '+str(bestWord[1]))
