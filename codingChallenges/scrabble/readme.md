# [Challenge] [Scrabble](https://www.sololearn.com/Discuss/1074860/challenge-scrabble)
You are given n words, and must output the word with the best score.

The score of a word is calculated like this (The max length of a word is 10 letters):

* e, a, i, o, n, r, t, l, s, u are equal 1 point
* d, g	2 points
* b, c, m, p	3 points
* f, h, v, w, y 4 points
* k 5 points
* j, x 8 points
* q, z 10 points

For example:
```
input { "This",  "Is", "a", "Cool" , "Challenge" }
output : Challenge
```
