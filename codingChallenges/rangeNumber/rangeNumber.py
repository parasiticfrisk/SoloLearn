#!/usr/bin/env python3
# August Frisk | (c) 2017
# Range Number Challenge

from itertools import permutations as p
def numR(N):
    x = []
    for y in p(N):
        z = 0
        for i in range(len(y)-1):
            z += abs(y[i]-y[i+1])
        x.append([z, y])
    return max(x)
print(*numR([8, 1, 7]))
print(*numR([2, 3, 2]))
print(*numR([1, 2, 2, 2, 3]))
print(*numR([1, 2, 3]))
