# 🏆🏆[CHALLENGE] [Range number](https://www.sololearn.com/Discuss/811374/challenge-range-number)🏆🏆
A list has the property of having a "range number".
A range number is calculated by finding the maximum possible total difference between any order of consecutive terms.

Write a function ```rangenum(N)``` where ```N``` is a list.
* Test cases:
```
rangenum([2,3,2])
>>> 2
# ([2,3,2] is the case)
rangenum([1,2,3])
>>> 3
# ([1,3,2] is the arrangement)
```
* New test cases:
```
rangenum([8,1,7])
>>> 13 ([8,1,7])
```
**Bonus:** give the permutation of the list that returns its range number
