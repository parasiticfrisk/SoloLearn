#!/usr/bin/env python3
# August Frisk | (c) 2017

'''
File Watcher

Parameters: str | list[str] filename(s), function callback, int | float wait_time

Create a file watching thread to check for modifications to one or more files.
Uses file metadata to comapre modification times.
Be careful when using a low wait_time as not to cause too many read/ write operations

Works well on linux systems. May not work in windows. Definitely doesn't work in sololearn
'''

import os, time, threading

running = True

def file_watcher(filename, callback, wait_time=5):
    def watch():
        m = os.stat(filename)
        lastAccessed = m[7]
        lastModified = m[8]
        while running:
            m = os.stat(filename)
            if m[7] > lastAccessed and m[8] > lastModified:
                callback({'type':'modify','file':filename})
                lastModified = m[8]
                lastAccessed = m[7]
            elif m[7] > lastAccessed:
                callback({'type':'access','file':filename})
                lastAccessed = m[7]
            time.sleep(wait_time)

    def watch_multiple():
        files = []
        for f in filename:
            m = os.stat(f)
            files.append({
                'last-accessed':m[7],
                'last-modified':m[8],
                'path':f
            })
        while running:
            for f in files:
                m = os.stat(f['path'])
                if m[7] > f['last-accessed'] and m[8] > f['last-modified']:
                    callback({'type':'modify','file':f['path']})
                    f['last-modified'] = m[8]
                    f['last-accessed'] = m[7]
                elif m[7] > f['last-accessed']:
                    callback({'type':'access','file':f['path']})
                    f['last-accessed'] = m[7]
                time.sleep(wait_time)
                
    if isinstance(filename, str):
        return threading.Thread(target=watch)
    if isinstance(filename, list):
        return threading.Thread(target=watch_multiple)

### Example:
# Example callback. I.e. The code to run when an event is triggered
def on_event(event):
    if event['type'] == 'modify':
        if event['file'] == 'path/to/file':
            do_stuff()
    if event['type'] == 'access':
        print('{} was accessed'.format(event['file']))
        
# Example watch single file
watcher1 = file_watcher('physics.py', on_event)

# Example watch multiple files
watcher2 = file_watcher(['/home/August/sand_piles.cpp', 'shaders/model.vert'], on_event)

# Start the watcher
watcher2.start()
running = False
