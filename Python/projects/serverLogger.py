#!/usr/bin/env python
# August Frisk | (c) 2017

# Logger script using paramiko

import os, glob, paramiko, datetime, hashlib

print('=' * 60)
hostname = raw_input('Insert hostname..')
uname = raw_input('Insert username..')
upass = raw_input('Insert passwd..')
local_path = '/home/logger'
logpath = [ '/var/log/',
 '/var/log/apache2/', 
 '/var/log/exim4/', 
 '/var/log/proftpd/']
backpath = [ '/var/backup/',
 '/var/backups/',
 '/var/www/httpd-cert/']
locname = []
now_date = datetime.date.today()
print("Logger script for " + hostname)

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
print("Connecting to server...")
try:        ssh.connect(hostname, username = uname, password = upass)
except Exception:
    print('Connection failed')
exit()

def filesize(arc):
    stdin, stdout, stderr = ssh.exec_command('du -h ' + arc)
    _size = stdout.read()#
    return _size
    
def create_checksum(path):
    fp = open(path)
    checksum = hashlib.sha256()
    while True:
        buffer = fp.read(8192)
        if not buffer:break
        checksum.update(buffer)
    fp.close()
    checksum = checksum.hexdigest()
    return checksum
    
def session_close():
    stdin, stdout, stderr =     ssh.exec_command('exit')
    bmessage = stdout.read().splitlines()
    return bmessage
    
def get_remote(rempath):
    file_counter = 0
    path_step = 0
    while path_step < len(rempath):
        try:
            print('=' * 60)
            print('Connection to directory...')
            print(rempath[path_step])
            locname=[]
            stdin, stdout, stderr = ssh.exec_command('ls ' + rempath[path_step]) 
            locname = stdout.read().splitlines()
        except Exception:
            print('Cannot access this directory. Please contact your administrator.')
           
step = 0
while step < len(locname):
    arc = rempath[path_step] + locname[step]
    print(filesize(arc))
    sftp = ssh.open_sftp()
    try:
        normaldir = rempath[path_step]  
        normaldir = normaldir[:-1]
        os.makedirs('logger' + normaldir)
    except OSError:
        pass
    try:
        sftp.get(arc, local_path + arc)
        file_counter += 1
    except Exception:
        file_counter -= 1
        pass
    sftp.close()
    step += 1
    path_step += 1
#   return file_counter
 
print('actions -> getlog, getbackup, complete')
action = raw_input('Input action...')
file_counter = 0
if action == 'getlog':
    file_counter = get_remote(logpath)
    
if action == 'getbackup':
    file_counter = get_remote(backpath)
    
if action == 'complete':
    complpath = logpath + backpath
    file_counter = get_remote(complpath)
    
if action == 'local_checksum':
    summ = create_checksum(raw_input())
    print(summ)
    print ('=' * 60)
    print ('Total files copied:', file_counter)
    print ('All operations complete!')
    print ('=' * 60)
