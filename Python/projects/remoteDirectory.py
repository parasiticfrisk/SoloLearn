#!/usr/bin/env python3
# August Frisk | (c) 2017
'''
Remote directory download

Pulls down file content of a remote directory and downloads all files meeting certain search criteria.
You will need the correct privellages to the directory
'''

import urllib.request
import re

# replace the remote_dir with the desired remote directory address
remote_dir = 'https://github.com/parasiticfrisk/SoloLearn/Python/'
url_path = urllib.request.urlopen(remote_dir)
real_path = url_path.read().decode('utf-8')

# replace the pattern with a RegEx proper syntax - currently set to find .py
file_pattern = re.compile("([\w\.-]+[\.]py)")
file_list = set(file_pattern.findall(real_path))
print(file_list)

for f in file_list:
    remote_file = urllib.request.urlopen(remote_dir + f)
    print('Copying file:', f, end='')
    local_file = open(f, 'wb')
    local_file.write(remote_file.read())
    local_file.close()
    print(' Done.')
    remote_file.close()
