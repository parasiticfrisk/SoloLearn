#!/usr/bin/env python3
# August Frisk | (c) 2017

import socket

help = """
INSTRUCTIONS:
Enter the name of a website (i.e. www.python.org)
The script will retreive the site's ip address.
"""
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

name = input('Name Of The Host:\n')
try:
    if True:
        msg_a = "Host Name:"
        msg_a += name
        msg = "The Host Address Is:"
        msg += socket.gethostbyname(str(name))
        print(msg_a + "\n" + msg)
    else:
        print(help)
except socket.gaierror as e:
    print(help)
    err_msg = "Error Is Defined As socket.gaierror Cause This Is Invalid Name\n"
    re_msg = "This Error Can't Be Defined As A String Object This Will Output\n"
    ty = "A Type Error"
    print(err_msg,re_msg,str(ty))
