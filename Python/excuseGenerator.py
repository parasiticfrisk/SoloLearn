#!/usr/bin/env python3
# August Frisk | (c) 2017

from random import randint

opening={
1:"I swear, ",
2:"Well, ",
3:"You see, ",
4:"Uhmm, ",
5:"Ehh, ",
6:"Hmmm, ",
7:"Yeah, ",
8:"Hey, "}

excuses={
1:"it's not a bug it's a feature",
2:"it worked on my machine",
3:"I tested it and it worked",
4:"its production ready",
5:"your browser must be caching the old content",
6:"that error means it was successful",
7:"the client fucked it up",
8:"the systems crashed and the code got lost",
9:"this code wont go into the final version",
10:"it's a compiler issue",
11:"it's only a  minor issue",
12:"this will take two weaks max",
13:"my code is flawless must be someone else's mistake",
14:"it worked a minute ago",
15:"that was not in the original specification",
16:"I was told to stop working on that when something important came up",
17:"You must have the wrong version",
18:"thats way beyond my paygrade",
19:"that's just an unlucky coincidence",
20:"I saw the new guy screw around with the systems",
21:"our servers must've been hacked",
22:"I wasnt given enough time",
23:"its the designers fault",
24:"it probably wont happen again",
25:"your expectations were unrealistic",
26:"everythings great on my end",
27:"thats not my code",
28:"its a hardware problem",
29:"its a firewall issue",
30:"its a character encoding issue",
31:"a third party api isnt responding",
32:"that was only supposed to be a placeholder",
33:"The third party documentation is wrong",
34:"that was just a temporary fix.",
35:"We outsourced that months ago.",
36:"that value is only wrong half of the time.",
37:"the person responsible for that does not work here anymore",
38:"That was literally a one in a million error",
39:"our servers couldn't handle the traffic the app was recieving",
40:"your machines processors must be too slow",
41:"your pc is too outdated",
42:"I haven't pushed the latest changes yet",
43:"that is a known issue with the programming language",
44:"it would take too much time and resources to rebuild from scratch",
45:"this is historically grown",
46:"users will hardly notice that",
47:"I will fix it"}

result=opening[randint(1,8)]+excuses[randint(1,47)]
print(result)
