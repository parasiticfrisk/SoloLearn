#!/usr/bin/env python3
# August Frisk | (c) 2017

# import system information
import platform, os, sys
print("OS: "+platform.system()+"\nMachine: "+platform.machine()+"\nVersion: "\
      +platform.version()+"\nUname: "+str(platform.uname())+"\nos.name: "+os.name+\
      "\nPython version: "+str(sys.version)+"\nPython version info: "+str(sys.version_info))
